package com.example.androidarchitecture.mvvm.view.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.androidarchitecture.R;
import com.example.androidarchitecture.mvvm.view.adapter.ViewPagerAdapter;

public class TalentActivity extends AppCompatActivity {

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.talent_activity);
        ViewPager viewPager = findViewById(R.id.viewPager);
        TabLayout tabLayout = findViewById(R.id.tabs);

        setupViewPagerAdapter(viewPager);
        tabLayout.setupWithViewPager(viewPager);
    }

    private void setupViewPagerAdapter(ViewPager viewPager) {
        ViewPagerAdapter viewPagerAdapter = new ViewPagerAdapter(getSupportFragmentManager());
        ListTalentFragment talentFragment = new ListTalentFragment();
        InscriptionTalentFragment inscriptionTalentFragment = new InscriptionTalentFragment();

        viewPagerAdapter.addFragment(talentFragment, "Talents");
        viewPagerAdapter.addFragment(inscriptionTalentFragment, "Inscription");

        viewPager.setAdapter(viewPagerAdapter);

    }
}

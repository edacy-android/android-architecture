package com.example.androidarchitecture.mvp.ui;

/**
 *
 */
public interface LoginPresenter {
    void onLoginButtonClick(String login, String mdp);
}

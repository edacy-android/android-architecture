package com.example.androidarchitecture.mvp.ui;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.EditText;

import com.example.androidarchitecture.R;

public class LoginActivity extends AppCompatActivity implements LoginView {
    private EditText loginEdit;
    private EditText mdpEdit;
    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.login);
        loginEdit = findViewById(R.id.login);
        mdpEdit = findViewById(R.id.mot_de_passe);

        final LoginPresenter presenter = new LoginPresenterImpl(this);
        findViewById(R.id.login_button).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String mdp = mdpEdit.getText().toString();
                String login = loginEdit.getText().toString();
                presenter.onLoginButtonClick(login, mdp);
            }
        });


    }

    @Override
    public void showProgressBar(boolean show) {
        //implement progressbar
    }

    @Override
    public void loginEditError(String message) {
        loginEdit.setError(message);
    }

    @Override
    public void loginMDPEditor(String message) {
        mdpEdit.setError(message);
    }
}

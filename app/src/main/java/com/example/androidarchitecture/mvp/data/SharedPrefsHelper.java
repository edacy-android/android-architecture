package com.example.androidarchitecture.mvp.data;

import android.content.Context;
import android.content.SharedPreferences;

public class SharedPrefsHelper {
    private static final String LOGIN_PREFS = "login_prefs";
    private  final String LOGIN = "login";
    private  final String MOT_DE_PASSE = "mot_de_passe";
    private SharedPreferences preferences;

    public SharedPrefsHelper(Context context) {
        this.preferences = context.getSharedPreferences(LOGIN_PREFS, Context.MODE_PRIVATE);
    }

    public String getLogin() {
        return preferences.getString(LOGIN, null);
    }

    public  String getMotDePasse() {
        return preferences.getString(MOT_DE_PASSE, null);
    }

    public void setLogin(String login) {
        preferences.edit().putString(LOGIN, login).apply();
    }

    public void setMotDePasse(String motDePasse) {
        preferences.edit().putString(MOT_DE_PASSE, motDePasse);
    }

}

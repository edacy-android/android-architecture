package com.example.androidarchitecture.mvp.ui;

/**
 * Cette interface represente la partie visible de l'interface login
 */
public interface LoginView {
    /**
     * @param show true show progress false if not
     */
    void showProgressBar(boolean show);

    /**
     * Show the message error in the login EditText
     * @param message error
     */
    void loginEditError(String message);

    /**
     * Show the message error in the mdp EditText
     * @param message error
     */
    void loginMDPEditor(String message);
}

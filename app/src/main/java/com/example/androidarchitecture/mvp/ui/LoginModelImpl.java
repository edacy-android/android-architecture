package com.example.androidarchitecture.mvp.ui;

import android.app.Application;

import com.example.androidarchitecture.App;
import com.example.androidarchitecture.mvp.data.SharedPrefsHelper;

public class LoginModelImpl implements LoginModel {




    @Override
    public void login(String login, String mdp) {
        App.getPrefsHelper().setLogin(login);
        App.getPrefsHelper().setMotDePasse(mdp);
    }
}

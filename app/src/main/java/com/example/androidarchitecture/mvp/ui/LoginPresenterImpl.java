package com.example.androidarchitecture.mvp.ui;

public class LoginPresenterImpl implements LoginPresenter {

    private LoginModel model;
    private LoginView view;
    public LoginPresenterImpl(LoginView loginView) {
        this.view = loginView;
        model = new LoginModelImpl();
    }

    @Override
    public void onLoginButtonClick(String login, String mdp) {
        if (login.equals("")) {
            view.loginEditError("Le login ne doit pas etre vite");
            return;
        }
        else if (mdp.equals("")) {
            view.loginMDPEditor("Le login ne doit pas etre vite");
            return;
        }
        else if (login.equals("") && mdp.equals("")) {
            view.loginEditError("Le login ne doit pas etre vite");
            view.loginMDPEditor("Le login ne doit pas etre vite");
            return;
        }
        model.login(login, mdp);
    }
}

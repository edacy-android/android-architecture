package com.example.androidarchitecture;

import android.app.Application;

import com.example.androidarchitecture.mvp.data.SharedPrefsHelper;

public class App extends Application {
    private  static SharedPrefsHelper prefsHelper = null;
    @Override
    public void onCreate() {
        super.onCreate();
        prefsHelper = new SharedPrefsHelper(getApplicationContext());
    }

    public static SharedPrefsHelper getPrefsHelper() {
        return prefsHelper;
    }
}
